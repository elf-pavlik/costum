<?php 
class ValidateGroupAction extends CAction{
	public function run(){

		if( !empty(Yii::app()->session["costum"]["slug"]) && 
			!empty($_POST["type"]) && 
			!empty($_POST["id"]) && 
			isset($_POST["valid"]) ){
			
			$elt = PHDB::findOneById( $_POST["type"], $_POST["id"], array("source", "links"));
			if($_POST["valid"] == "true"){
				if( !empty($elt["source"]["toBeValidated"]) ) {
					unset($elt["source"]["toBeValidated"][Yii::app()->session["costum"]["slug"]]);
					if(count($elt["source"]["toBeValidated"]) == 0)
						unset($elt["source"]["toBeValidated"]);
				}
			}else{
				if( empty($elt["source"]["toBeValidated"]) )
					$elt["source"]["toBeValidated"] = array();
				$elt["source"]["toBeValidated"][Yii::app()->session["costum"]["slug"]] = true ;
				
			}
			$query=array("source" => $elt["source"]);
			$res= PHDB::update( $_POST["type"], 
								array("_id" => new MongoId($_POST["id"])), 
								array('$set'=>$query));

			$sub = array();
			foreach ($elt["links"] as $kL => $vL) {
				foreach ($vL as $key => $value) {
					if(!empty($value["type"]) && $value["type"] != Person::COLLECTION){

						$where = array(	"_id" => new MongoId($key),
										"source.keys" => array('$in' => array(Yii::app()->session["costum"]["slug"] ) ) );
						$subElt = PHDB::findOne($value["type"], $where, array("name","source", "links"));

						if(!empty($subElt)){
							if($_POST["valid"] == "true"){
								if( !empty($subElt["source"]["toBeValidated"]) ) {
									unset($subElt["source"]["toBeValidated"][Yii::app()->session["costum"]["slug"]]);
									if(count($subElt["source"]["toBeValidated"]) == 0)
										unset($subElt["source"]["toBeValidated"]);
								}
							}else{
								if( empty($subElt["source"]["toBeValidated"]) )
									$subElt["source"]["toBeValidated"] = array();
								$subElt["source"]["toBeValidated"][Yii::app()->session["costum"]["slug"]] = true ;
								
							}

							//$sub[$key] = $subElt;
							$querySub=array("source" => $subElt["source"]);
							$sub[$key] = PHDB::update( $value["type"], 
												array("_id" => new MongoId($key)), 
												array('$set'=>$querySub) );
						}
						
						

						
					}
				}
			}



			Rest::json(array("result"=>true, "elt" => $elt, "sub" => $sub));
		}
	}
}
?>