<div class="col-xs-12">
    <h1 class="text-center">Acteurs Publics</h1>

    <div class="col-xs-12 margin-top-20">
        <?php 
        
        $params = array(
            "poiList" => PHDB::find(Poi::COLLECTION, 
                        array( "parent.".Yii::app()->session["costum"]["contextId"] => array('$exists'=>1), 
                               "parent.".Yii::app()->session["costum"]["contextId"].".type"=>Yii::app()->session["costum"]["contextType"],
                               "type"=>"cms") ),
            "listSteps" => array("one","two","three","four","five","six")
        );

        foreach ($params['poiList'] as $id => $value) {
            $documents  = PHDB::find(Document::COLLECTION, array('id' => $id, "type" => Poi::COLLECTION ));
            if( count($documents) )
            {
                $params['poiList'][$id]["documents"] = array();
                foreach ( $documents as $key => $doc ) {
                  $params['poiList'][$id]["documents"][] = array (
                    "name" => $doc['name'], 
                    "doctype" => $doc['doctype'],  
                    "path" => Yii::app()->baseUrl."/".Yii::app()->params['uploadUrl'].$doc["moduleId"]."/".$doc["folder"]."/".$doc['name']
                  );
                }
            }
        }
        
        //var_dump($params);
        echo $this->renderPartial("costum.views.tpls.wizard",$params,true);
        ?>
    </div>
</div>

