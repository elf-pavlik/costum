<?php 
$cssJS = array(
      '/plugins/jQuery-Knob/js/jquery.knob.js',
      '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
      //'/plugins/jQuery-Smart-Wizard/styles/smart_wizard.css',
    // SHOWDOWN
    '/plugins/showdown/showdown.min.js',
    // MARKDOWN
    '/plugins/to-markdown/to-markdown.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl); 

//echo "<script type='text/javascript'> alert(' home ".@Yii::app()->session["costum"]["slug"]." : ".@Yii::app()->session["costum"]["contextType"]." : ".@Yii::app()->session["costum"]["contextId"]."'); </script>";


if(Yii::app()->session["costum"]["contextType"] && Yii::app()->session["costum"]["contextId"]){
    $el = Element::getByTypeAndId(Yii::app()->session["costum"]["contextType"], Yii::app()->session["costum"]["contextId"] );

    $poiList = PHDB::find(Poi::COLLECTION, 
                    array( "parent.".Yii::app()->session["costum"]["contextId"] => array('$exists'=>1), 
                           "parent.".Yii::app()->session["costum"]["contextId"].".type"=>Yii::app()->session["costum"]["contextType"],
                           "type"=>"cms") );
}

?>
<div class="">



<?php /*
if(false ){//!@$el["costum"]){
  $tagBarList = array(
    array("icon"=>"fa-user","color"=>"#fff"),
    array("icon"=>"fa-group","color"=>"#fff"),
    array("icon"=>"fa-calendar","color"=>"#fff"),
    array("img"=>Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/badge.png"));
    echo $this->renderPartial("costum.views.tpls.tagBar",
                        array(  "list" => $tagBarList,
                                "borderColor" => "white",
                                "bgColor" => "#22252A",
                                 ),true ); 
}
?>  

<div class="col-xs-12 no-padding support-section text-center">
  <br/><br/>
  <h2 id="carteCTE"><i class="fa fa-group"></i> acteurs publics</h2>
  <div class="col-xs-12 padding-20 text-explain" >
    <?php 
      $params = array(
          "poiList" => $poiList,
          "listSteps" => array("1","2","3","4","5","6"),
          "color1" => "#354fff"
      );
      //var_dump($params);
      echo $this->renderPartial("costum.views.tpls.wizard",$params);
      ?>

  </div>
</div>

<div class="col-xs-12 no-padding support-section text-center">
  <br/><br/>
  <h2 id="carteCTE"><i class="fa fa-money"></i> acteurs sociaux économique</h2>
  <div class="col-xs-12 padding-20 text-explain" >
    <?php 
      $params = array(
          "poiList" => $poiList,
          "listSteps" => array("one","two","three","four","five","six")
      );
      //var_dump($params);
      echo $this->renderPartial("costum.views.tpls.wizard",$params,true);
      ?>
    
  </div>
</div>
*/?>

<div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">
  
    <div class="col-xs-12 text-center margin-bottom-50" style="padding:0px;">
    <?php 
    $banner = Yii::app()->getModule("costum")->getAssetsUrl()."/images/ctenat/banner.jpg";
    if(@Yii::app()->session["costum"]["metaImg"]){
      if(strrpos(Yii::app()->session["costum"]["metaImg"], "http" ) === false && strrpos( Yii::app()->session["costum"]["metaImg"], "/upload/" ) === false )
        $banner = Yii::app()->getModule("costum")->getAssetsUrl().Yii::app()->session["costum"]["metaImg"] ;
      else 
        $banner = Yii::app()->session["costum"]["metaImg"];
    }
    ?>
    <img class="img-responsive"  style="margin:auto;background-color: black;" src='<?php echo $banner ?>'/> 
  <!--<div class="col-md-12 col-lg-12 col-sm-12 imageSection no-padding" 
     style=" position:relative;">-->
  <div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="background-color:#1F2532; max-width:100%; float:left;">
    <div class="col-xs-12 no-padding" style="margin-top:100px;"> 
      <div class="col-xs-12 no-padding">
        <div class="col-md-12 col-sm-12 col-xs-12 padding-20" style="padding-left:100px;background-color: #f6f6f6; min-height:400px;">
          <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="margin-top:-80px;margin-bottom:-80px;background-color: #fff;font-size: 14px;z-index: 5;">
            <div class="col-xs-12 font-montserrat ourvalues" style="text-align:center;">
              <h3 class="col-xs-12 text-center">
                
                <small style="text-align: left">
                <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
                  <bloquote style="font: 10 25px/1 'Pacifico', Helvetica, sans-serif;
  color: #2b2b2b;
  text-shadow: 4px 4px 0px rgba(0,0,0,0.1);">


<!--
<span class="main-title" style="font-family:'Pacifico', Helvetica, sans-serif;color:#65BA91;font-size: 50px; font-style: italic;">#iciOnAccélère</span><br>
<br/>
-->

  « Le contrat de transition écologique illustre la méthode souhaitée par le
Gouvernement pour accompagner les territoires : une coconstruction avec les
élus, les entreprises et les citoyens qui font le pari d’une transition écologique
génératrice d’activités économiques et d’opportunités sociales. »</bloquote>

              </h3>
              <div  style="text-align: right; font-size: 1.4em;padding-right:30px;">
                <b>Emmanuelle Wargon</b>,<br/>
                secrétaire d’État auprès du ministre d’État, ministre de la Transition écologique et solidaire
                  </small>
              </div>
              <br/>


              
              <hr style="width:40%; margin:20px auto; border: 4px solid #6BB3C1;">


              
<iframe width="560" height="315" src="https://www.youtube.com/embed/KDnSIlq44fw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                




                <hr style="width:40%; margin:20px auto; border: 4px solid #6BB3C1;">
              





              <div class="col-md-10 col-md-offset-1 col-xs-12">
                
                <span class="text-explain">
                  <br/>
                  <h2 style="color: #1A5D98">UN CONTRAT ADAPTE AU TERRITOIRE</h2>


                  Lancés en 2018, les <b>contrats de transition écologique</b>
                  (CTE) traduisent les engagements environnementaux
                  pris par la France (Plan climat, COP21, One Planet
                  Summit) au niveau local. Ce sont des outils au
                  service de la transformation écologique de territoires
                  volontaires, autour de projets durables et concrets.

                  <br/><span class="bullet-point"></span><br/>

                  Mis en place par une ou plusieurs intercommunalités,
                  le CTE est coconstruit à partir de projets locaux, entre
                  les collectivités locales, l’État, les entreprises, les
                  associations... Les territoires sont accompagnés aux
                  niveaux technique, financier et administratif, par les
                  services de l’État, les établissements publics et les
                  collectivités. Signé après six mois de travail, le CTE fixe
                  un programme d’actions avec des engagements précis
                  et des objectifs de résultats.

                <hr style="width:40%; margin:20px auto; border: 4px solid #6BB3C1;">
                <br/>
                <h2 style="color: #1A5D98">TROIS OBJECTIFS</h2>
                <br/>
                <b>1) Démontrer  par  l’action  que  l’écologie  est  un  moteur  de  l’économie,  et  développer  l’emploi  local  par  la  transition  écologique</b>  (structuration  de filières, création de formations).

                <br/><span class="bullet-point"></span><br/>

                <b>2) Agir avec tous les acteurs du territoire, publics comme privés</b> pour traduire concrètement la transition écologique.

                <br/><span class="bullet-point"></span><br/>

                <b>3) Accompagner de manière opérationnelle les
                situations de reconversion industrielle d’un ter-
                ritoire</b> (formation professionnelle, reconversion de
                sites).



                </span>

                <hr style="width:40%; margin:20px auto; border: 4px solid #6BB3C1;">
<!-- 
                Pour préparer votre candidature, veillez à télécharger la présentation du contenu attendu. <a href="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/Formulaire_candidature_V2.pdf" style="background-color: #65BA91" class="btn" target="_blank"><i class="fa fa-download"></i></a><br/><br/>


                  <a href="javascript:;" onclick="$('#modalCandidater').modal('show');" class="btn-main-menu col-sm-8 col-sm-offset-2 margin-top-20" style="background-color: #65BA91">
                    <div class="text-center">
                        <div class="col-md-12 no-padding text-center">
                            <h4 class="no-margin uppercase">
                              <i class="fa fa-hand-point-right faa-pulse"></i>
                              <?php echo Yii::t("home","Devenir un territoire CTE") ?>
                            </h4>
                        </div>
                    </div>
                </a> -->
            <div class="text-center">
                    <div class="col-md-12 no-padding text-center text-red">
                        <h4 class="no-margin uppercase">
                L'appel à manifestation d'intérêt des contrats de transition écologique s'est tenu entre le 2 avril et le 27 mai 2019. Il est désormais clos.
                </h4></div>
            </div>

              </div>
            </div>
            <div class="text-center col-xs-12">
            <br/><br/>
            <img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/city.png"></div>
        </div>
          </div>

        </div>

      </div>
      

      


      <div class="col-xs-12 no-padding support-section text-center">
        <br/><br/>
        <h2 id="carteCTE"><i class="fa fa-map-marker"></i> CARTE DE FRANCE DES CTE</h2>
        <div class="col-xs-12 padding-20 text-explain" >
          <img class="img-responsive" style="margin:auto" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/cteDeFrance.png">
        </div>
      </div>
        




      <div class="col-xs-12 no-padding support-section">
        <h2 id="acteurCTE"><i class="fa fa-handshake-o"></i> Quels acteurs ENGAGÉs ?</h2>
        <div class="col-xs-12 padding-20 text-explain" >
          <img class="img-responsive" style="margin:auto"  src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/elaborer.png">
<br/>
          <div class="col-sm-12" style="color:#65BA91; font-weight: bold; ">

            Une fois signé, le CTE est suivi et mis en œuvre par l’EPCI avec le soutien de l’État, de la région et du département.<br/>
            
            Le CTE devient ainsi un outil au service des projets portés par les citoyens, les associations et les entreprises.<br/><br/>
            <img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/badge.png" style="margin:auto">
          </div>
          
          
          
          <br/>
          <!-- 
          <a href="javascript:;" onclick="$('#modalCandidater').modal('show');" class="btn-main-menu col-sm-8 col-sm-offset-2 margin-top-20" style="background-color: #65BA91">
              <div class="text-center">
                  <div class="col-md-12 no-padding text-center">
                      <h4 class="no-margin uppercase">
                        <i class="fa fa-hand-point-right faa-pulse"></i>
                        <?php echo Yii::t("home","Devenir un territoire CTE") ?>
                      </h4>
                  </div>
              </div>
          </a> -->

        </div>
      </div>


      <div class="col-xs-12 no-padding support-section">
        <h2><i class="fa fa-calendar"></i> QUEL CALENDRIER ?</h2>
        <div class="col-xs-12  text-explain">
          
        <img style="margin:auto" class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/cal.png">
        
        
        
        </div>
        </div>
      </div>



      <div class="col-xs-12 no-padding support-section">
        
        <div class="col-xs-12 no-padding">
          
          
          <h3 style="color: #1A5D98">Toutes les informations et la carte de France des CTE <a href="http://www.ecologique-solidaire.gouv.fr/contrat-transition-ecologique" target="_blank">ici</a> </h3>
          
          <img width=200 src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/planClimat.png">

          <img width=200 src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/1200px-Ministère_de_la_Transition_Écologique_et_Solidaire_(depuis_2017).svg.png">

          <br/><br/>
        <img style="margin:auto" class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/city.png">

        </div>

      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  jQuery(document).ready(function() {
    setTitle("CTE : Contrat de Transition écologique");
  });
</script>


<div class="portfolio-modal modal fade" id="modalCandidater" tabindex="-1" role="dialog" aria-hidden="true">
    <form class="modal-content form-email box-email padding-top-15"  >
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <span class="name hidden" >
                        <?php if(Yii::app()->params["CO2DomainName"] == "kgougle"){ ?>
                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/KGOUGLE-logo.png" height="60" class="inline margin-bottom-15">
                       <?php } else { ?>
                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/CO2r.png" height="100" class="inline margin-bottom-15">
                        <?php } ?>
                    </span>
                    <h4 class=" no-margin" style="color: #1A5D98; margin-top:-5px!important;">Vous souhaitez rejoindre l’aventure CTE ?</h4><br>
                    <hr>
                    
                </div>
            </div>
            <div class="col-md-8 col-md-offset-2 text-left">
                Fort d’une expérimentation d’une année et demie sur une vingtaine de territoires diversifiés en métropole et en outre-mer, le ministère de la transition écologique et solidaire souhaite à présent déployer le dispositif en sélectionnant une première promotion d’une quarantaine de nouveaux territoires sur lesquels l’élaboration d’un CTE sera lancée en juillet 2019. 
                <br/><br/>
                Le projet proposé doit :
                <ul>
                <li>être le fruit d'une démarche volontaire </li>
                <li>être porté par le ou les président(es) de la collectivité territoriale (ou pays, syndicat, etc.) </li>
                <li>avoir pour ambition de mener des actions concrètes de transition écologique autours d’un fil rouge, dans un ou plusieurs domaines (économie circulaire, biodiversité, eau, énergie, agriculture…)</li>
                <li>associer des porteurs de projets socio-économiques, parties intégrantes du projet.</li>
                </ul><br/>

                Pour candidater, il vous suffit de remplir le formulaire suivant accessible du <b>2 avril 2019 au 27 mai 2019. </b>
                <br/><br/>
                Pour toute demande, vous pouvez contacter l'équipe nationale de coordination des contrats de transition écologique à cette adresse mail : <b class="text-blue">cte@developpement-durable.gouv.fr</b>
                <br/><br/>
                <a href="javascript:;" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Yii::t("common","Back") ?></a>

                <button data-dismiss="modal" data-form-type="project" class="btn-open-form btn btn-success text-white pull-right forgotBtn"><i class="fa fa-sign-in"></i> Candidater</button>
                
                
                <div class="col-md-12 margin-top-50 margin-bottom-50"></div>     
        </div>
    </form>
</div>


<?php 
/*
- search filter by src key 
- admin candidat privé only
- app carte 
*/
 ?>

<script type="text/javascript">

  jQuery(document).ready(function() {

    $.each($(".markdown"), function(k,v){
        descHtml = dataHelper.markdownToHtml($(v).html()); 
        $(v).html(descHtml);
    });
    contextData = {
        id : "<?php echo Yii::app()->session["costum"]["contextId"] ?>",
        type : "<?php echo Yii::app()->session["costum"]["contextType"] ?>",
        name : "<?php echo $el['name'] ?>",
        slug : "<?php echo $el['slug'] ?>",
        profilThumbImageUrl : "<?php echo $el['profilThumbImageUrl'] ?>"
    };
    
});

</script>

