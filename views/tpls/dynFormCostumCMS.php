<script type="text/javascript">
var dynFormCostumCMS = {
    "beforeBuild":{
        "properties" : {
            "structags" : {
                "inputType" : "tags",
                "placeholder" : "Structurer le contenu",
                "values" : null,
                "label" : "Structure et Hierarchie (parent ou parent.enfant)"
            },
            "documentation" : {
                "inputType" : "uploader",
                "label" : "Document associé (5Mb max)",
                "showUploadBtn" : false,
                "docType" : "file",
                "itemLimit" : 5,
                "contentKey" : "file",
                "order" : 8,
                "domElement" : "documentationFile",
                "placeholder" : "Le pdf",
                "afterUploadComplete" : null,
                "template" : "qq-template-manual-trigger",
                "filetypes" : [
                    "pdf","xls","xlsx","doc","docx","ppt","pptx","odt","ods","odp", "csv","png","jpg","jpeg","gif"
                    ]
            }
        }
    },
    "onload" : {
        "actions" : {
            "setTitle" : "CMS Data",
            "html" : {
                "nametext>label" : "Titre de la section",
                "infocustom" : "<br/>Gérez votre contenu vous meme"
            },
            "presetValue" : {
                "type" : "cms"
            },
            "hide" : {
                "locationlocation" : 1,
                "breadcrumbcustom" : 1,
                "urlsarray" : 1,
                "imageuploader":1,
                "tagstags":1
            }
        }
    }
};
</script>