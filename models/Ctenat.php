<?php

class Ctenat {
	const COLLECTION = "costums";
	const CONTROLLER = "costum";
	const MODULE = "costum";
	
    public static $dataBinding_allProject  = array(
        "id"     => array("valueOf" => "id"),
        "type"     => "Project",
        "name"      => array("valueOf" => "name"),
		"siren"     => array("valueOf" => "siren"),
		"why"     => array("valueOf" => "why"),
		"nbHabitant"     => array("valueOf" => "nbHabitant"),
		"dispositif"     => array("valueOf" => "dispositif"),
		"planPCAET"     => array("valueOf" => "planPCAET"),
		"singulartiy"     => array("valueOf" => "singulartiy"),
		"caracteristique"     => array("valueOf" => "caracteristique"),
		"actions"     => array("valueOf" => "actions"),
		"economy"     => array("valueOf" => "economy"),
		"inCharge"     => array("valueOf" => "inCharge"),
		"autreElu"     => array("valueOf" => "autreElu"),
		"contact"     => array("valueOf" => "contact"),
        "url"       => array("valueOf" => array(
                                // "pdf"           => array(   "valueOf"   => '_id.$id', 
                                //                                 "type"  => "url", 
                                //                                 "prefix"   => "/co2/export/pdfelement/type/projects/id/",
                                //                                 "suffix"   => "" ),
                                "website"       => array(   "valueOf" => 'url'))),
        "address"   => array("parentKey"=>"address", 
                             "valueOf" => array(
                                    "@type"             => "PostalAddress", 
                                    "streetAddress"     => array("valueOf" => "streetAddress"),
                                    "postalCode"        => array("valueOf" => "postalCode"),
                                    "addressLocality"   => array("valueOf" => "addressLocality"),
                                    "codeInsee"         => array("valueOf" => "codeInsee"),
                                    "addressRegion"     => array("valueOf" => "addressRegion"),
                                    "addressCountry"    => array("valueOf" => "addressCountry")
                                    )),
        "startDate"     => array("valueOf" => "startDate"),
        "endDate"       => array("valueOf" => "endDate"),
        "geo"   => array("parentKey"=>"geo", 
                             "valueOf" => array(
                                    "@type"             => "GeoCoordinates", 
                                    "latitude"          => array("valueOf" => "latitude"),
                                    "longitude"         => array("valueOf" => "longitude")
                                    )),

    );

    public static $dataHead_allProject  = array(
        "type"     => "Type",
        "name"      => "Nom",
        "siren"     => "Siren",
        "why"     => "Pourquoi souhaitez-vous réaliser un CTE ? Quelles sont vos attentes vis-à-vis d’un CTE ?",
        "nbHabitant"     => "Nombre d'habitants concernés?",
        "dispositif"     => "Quels sont les dispositifs actuellement mis en place sur tout ou partie du territoire (contractuels et/ou en lien avec la transition écologique) ?",
        "planPCAET"     => "Avez élaboré un Plan climat-air-énergie territorial (PCAET) sur votre territoire ?",
        "singulartiy"     => "Qu'est ce qui constitue la singularité de votre territoire ? Quel est le contexte économique et quels sont ses enjeux en termes de transition écologique ?",
        "caracteristique"     => "Quelles sont les caractéristiques de votre territoire sur lesquelles vous agissez/vous souhaiteriez agir dans le cadre du CTE ? (exemples : problèmes majeurs/risques auxquels vous êtes confrontés, thèmes sur lesquels vous vous êtes engagés à agir)",
        "actions"     => "Quelles sont les actions envisagées dans votre projet de CTE ? Décrivez au moins 3 pistes d’actions.",
        "economy"     => "Quels acteurs socio-économiques prévoyez-vous de mobiliser pour le portage d’actions du CTE ?",
        "inCharge"     => "Qui serait en charge du projet au sein de la collectivité (chargé de mission, équipe dédiée, élu...) ?",
        "autreElu"     => "Autres élus engagés politiquement dans la réussite du projet (député, sénateur, maires) ?",
        "contact"     => "Nom du référent local, Adresse postale, numéro de téléphone et adresse mail ?",
        // "url.communecter"     => "Url ctenat",
        // "url.pdf"     => "PDF",
        "url.website"     => "Site internet",
        "address.streetAddress"     => "Rue",
        "address.postalCode"     => "Code Postal",
        "address.addressLocality"     => "Ville",
        "address.codeInsee"     => "Insee",
        "address.addressCountry"     => "Code Pays",
        "geo.latitude"     => "Latitude",
        "geo.longitude"     => "Longitude",

    );
	
    // public static function prepData($params){
    //     if($params["collection"] == Project::COLLECTION){
    //     	if(!empty($params["scope"] )){
    //     		foreach ($params["scope"] as $key => $value) {
    //             	$params["name"] = $value["name"];
    //             }
    //     	}
            
    //     }
    //     return $params;
    // }

}
?>