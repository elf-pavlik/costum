<?php

class Costum {
	const COLLECTION = "costum";
	const CONTROLLER = "costum";
	const MODULE = "costum";
	
	//used in initJs.php for the modules definition
	public static function getConfig($context=null){
		return array(
			"collection"    => self::COLLECTION,
            "controller"   => self::CONTROLLER,
            "module"   => self::MODULE,
            "categories" => CO2::getModuleContextList(self::MODULE, "categories", $context),
       		"lbhp" => true
		);
	}

    //$ctrl if == null we are jsut initing the session data, no rendering 
    //$id 
    // if given with a type it concerns an element
    // otherwise it's just costum can be the slugname or the digital id 
    //$type : 
        // if given with an id it concerns an element
    //$slug 
        // it's the slug of an element
    //$init this action can render a result for a page request
    public static function init($ctrl, $id=null,$type=null,$slug=null){
        //$params = CO2::getThemeParams();
        //Yii::app()->session['paramsConfig']=$params;
        $elParams = ["shortDescription", "profilImageUrl", "profilThumbImageUrl", "profilMediumImageUrl","profilBannerUrl", "name", "tags", "description","costum"];

        $tmp = array();
        //ex : http://127.0.0.1/ph/costum/co/index/slug/cocampagne
        //le slug appartient à un élément
        if(isset($slug)){
            $el = Slug::getElementBySlug($slug, $elParams );
            $tmp['contextType'] = $el["type"];
            $tmp['contextId'] = $el["id"];
            if(@$el["el"]["costum"]){
                $id = (@$el["el"]["costum"]['id']) ? $el["el"]["costum"]['id'] : $el["el"]["costum"]['slug'];
            }
            else {
                //generate the costum tag on the element with a default COstum template 
                //can be changed in the costum admin 
                $id = "cocampagne"; 
            }

        } 
        //ex : http://127.0.0.1/ph/costum/co/index/id/xxxx/type/organizations
        //l'id et le type permettent de retrouver un élément
        else if(isset($type) && isset($id)){
            $el = array("el"=>Element::getByTypeAndId($type, $id, $elParams ));
            $tmp['contextType'] = $type;
            $tmp['contextId'] = $id;
            if(@$el["el"]["costum"]){
                $id = (@$el["el"]["costum"]['id']) ? $el["el"]["costum"]['id'] : $el["el"]["costum"]['slug'];
            }
            
        }
        //ex : http://127.0.0.1/ph/costum/co/index/id/cocampagne
        //l'id est celui du costum
        else 
            $id=(!empty($id)) ? $id : @$_GET["id"];

        // var_dump($id);
        //  exit;
        if(!empty($id)){
            if(strlen($id) == 24 && ctype_xdigit($id)  )
                $c = PHDB::findOne( Costum::COLLECTION , array("_id"=> new MongoId($id)));
            else 
                $c = PHDB::findOne( Costum::COLLECTION , array("slug"=> $id));
        }else if(@$_GET["host"]){
            $c = PHDB::findOne( Costum::COLLECTION , array("host"=> $_GET["host"]));
        }

        // var_dump($c);
        //  exit;

        if(@$c && !empty($c)){ 
            if(isset($c["redirect"])){
               // $slug=explode(".", $_GET["el"])[1];
                $redirect = PHDB::findOne( Costum::COLLECTION , array("slug"=> $c["redirect"]));
                $url = (isset($redirect["host"])) ? "https://".$redirect["host"] : Yii::app()->createUrl("/costum/co/index/id/".$redirect["slug"]);
                header('Location: '.$url);//Yii::app()->createUrl("/costum/co/index/id/".$redirect["slug"]));
                exit;
            }
            if(!isset($slug)){
                $el = Slug::getElementBySlug($c["slug"], $elParams );
                $slug = $c["slug"];
            }
            
            if(isset($tmp['contextType'])){
                $c['contextType'] = $tmp['contextType'];
                $c['contextId'] = $tmp['contextId'];
            }
// var_dump($c);
//          exit;
            if(!empty($el["el"]))
            {
                $element=array(
                    "contextType" => $el["type"],
                    "contextId" => $el["id"],
                    "title"=> $el["el"]["name"],
                    "description"=> @$el["el"]["shortDescription"],
                    "tags"=> @$el["el"]["tags"],
                    "isCostumAdmin"=>false,
                    "assetsUrl"=> Yii::app()->getModule("costum")->getAssetsUrl(true),
                    "url"=> "/costum/co/index/id/".$c["slug"] );
                

                if( @$el["el"]["profilImageUrl"] ) 
                    $c["logo"] = Yii::app()->createUrl($el["el"]["profilImageUrl"]);
                
                if(@$c["logoMin"])
                    $c["logoMin"] = Yii::app()->getModule( "costum" )->getAssetsUrl().$c["logoMin"];
                
                $c["admins"]= Element::getCommunityByTypeAndId($el["type"], $el["id"], Person::COLLECTION,"isAdmin");
                $links = (!empty($el["links"]) ? $el["links"] : null );
                $c["isMember"]= Link::isLinked($el["id"], $el["type"], Yii::app()->session["userId"],$links);
                if(@Yii::app()->session["userIsAdmin"] && !@$c["admins"][Yii::app()->session["userId"]])
                    $c["admins"][Yii::app()->session["userId"]]=array("type"=>Person::COLLECTION, "isAdmin"=>true);
                if(@$c["admins"][Yii::app()->session["userId"]])
                    Yii::app()->session['isCostumAdmin']=true;

                $c = array_merge( $c , $element );
                //possibly overload the costum template 

                if( isset( $el["el"]["costum"] ) && $c["slug"] == @$el["el"]["costum"]["slug"]  ){
                    unset($el["el"]["costum"]['id']);
                    unset($el["el"]["costum"]['slug']);
                    $c = self::combine($c,$el["el"]["costum"] ); 
                }

                
                /* metadata */
                //print_r($this->getController()->module);exit;
                //Costum::initMetaData($c, $this->getController()->module);
                if(isset($c["metaDesc"]))
                    $shortDesc=$c["metaDesc"];
                else{
                    $shortDesc =  @$c["shortDescription"] ? $c["shortDescription"] : "";
                        if($shortDesc=="")
                            $shortDesc = @$c["description"] ? $c["description"] : "";
                }
                if(isset($ctrl)){
                    $ctrl->module->description = $shortDesc;
                    $ctrl->module->pageTitle = (isset($c["metaTitle"])) ? $c["metaTitle"] : @$c["title"];
                    $ctrl->module->author = (isset($c["metaTitle"])) ? $c["metaTitle"] : @$c["title"];
                    if(isset($c["metaKeywords"]))
                        $ctrl->module->keywords = $c["metaKeywords"];
                    else
                       $ctrl->module->keywords = (@$c["tags"]) ? implode(",", @$c["tags"]) : "";
                    if (isset($c["favicon"])) {
                        $mod = $ctrl->module->id;
                        //ex images can be given 
                        if( substr_count($c["favicon"], '#') ){
                            $pieces = explode("#", $c["favicon"]);
                            $mod = $pieces[0];
                            $c["favicon"] = $pieces[1];
                        }
                        $ctrl->module->favicon = Yii::app()->getModule( $mod )->getAssetsUrl().$c["favicon"];
                    }
                    if (@$c["metaImg"]) {
                        $mod = $ctrl->module->id;
                        //ex images can be given 
                        if( substr_count($c["metaImg"], '#') ){
                            $pieces = explode("#", $c["metaImg"]);
                            $mod = $pieces[0];
                            $c["metaImg"] = $pieces[1];
                        }
                        $ctrl->module->image = Yii::app()->getModule( $mod )->getAssetsUrl().$c["metaImg"];
                    }
                    else if(@$c["logo"]){
                        $ctrl->module->image = $c["logo"];
                    }
                    if(@$c["logo"]){
                        $ctrl->module->image = $c["logo"];
                    }
                    $ctrl->module->relCanonical = (@$c["host"]) ? $c["host"] : Yii::app()->createUrl($c["url"]);   
                }
                // Besoin d'approfindir ce sujet des jsons et de leurs utilisations
                // Ici je l'ai utilisé pour injecter des tags spécifiques au pacte / on pourrait aussi penser au catégories d'événement
                //var_dump($c["tags"]); exit;
                if(isset($c["json"])){
                    foreach($c["json"] as $k => $v){
                        
                        $c[$k]=Costum::getContextList($c["slug"],$v);
                        if($k=="tags")
                            $c["request"]["searchTag"]=$c["tags"];
                            //$c["request"]["searchTag"]=[$c["tags"]];          
                    }
                }
                
                if( isset($c["searchTag"]) ) 
                    $c["request"]["searchTag"]=[$c["searchTag"]];
                if(isset(Yii::app()->session["userId"])){
                    $c=Costum::checkUserPreferences($c, Yii::app()->session["userId"]);
                }
                if(isset($c["searchTag"])) $c["request"]["searchTag"]=[$c["searchTag"]];
                if(isset($c["scopeSelector"]))
                    $c=Costum::initScopeSelector($c);
                /* metadata */
            }
            
            $c["request"]["sourceKey"] = [$c["slug"]];
            // $c["contextId"] = $ctxId;
            // $c["contextType"] = $ctxType;

            Yii::app()->session['costum'] = $c;

            //if(!@Yii::app()->session['paramsConfig'] || empty(Yii::app()->session['paramsConfig'])) 
            Yii::app()->session['paramsConfig'] = CO2::getThemeParams(); 

            Yii::app()->session["paramsConfig"]=Costum::filterThemeInCustom(Yii::app()->session["paramsConfig"]);
        }    
    }

    //merge and update existing fields 
    // to be used when applying specific changes defined on an element into a template costum object
    public static function combine($a1, $a2) {
        foreach($a2 as $k => $v) {
            if(is_array($v)) {
                if(!isset($a1[$k]))
                    $a1[$k] = null;

                $a1[$k] = combine($a1[$k], $v);
            } else {
                $a1[$k] = $v;
            }
        }
        return $a1;
    }

	/**
	 * get a Poi By Id
	 * @param String $id : is the mongoId of the poi
	 * @return poi
	 */
	public static function getById($id) { 
	  	$classified = PHDB::findOneById( self::COLLECTION ,$id );
	  	//$classified["parent"] = Element::getElementByTypeAndId()
	  	$classified["typeClassified"]=@$classified["type"];
	  	$classified["type"]=self::COLLECTION;
	  	$where=array("id"=>@$id, "type"=>self::COLLECTION, "doctype"=>"image");
	  	$classified["images"] = Document::getListDocumentsWhere($where, "image");//(@$id, self::COLLECTION);
	  	return $classified;
	}

	/**
	* Function called by @self::filterThemeInCustom in order to treat params.json of communecter app considering like the config
	* This loop permit to genericly update value, add values or delete value in the tree
	* return the communecter entry filtered by costum expectations
	**/
	public static function checkCOstumList($check, $filter,$k=null){
        $newObj=array();

        $arrayInCustom=["label","subdomainName","placeholderMainSearch","icon","height","imgPath","useFilter","slug","formCreate", "setParams", "initType", "inMenu","types","actions", "dataTarget", "dynform", "id", "filters", "tagsList", "calendar"];
        
        foreach($check as $key => $v){
            
            if(!empty($v)){
                $newObj[$key]=(isset($filter[$key])) ? $filter[$key] : $v;
                $checkArray=true;
                foreach($arrayInCustom as $entry){
                    if(isset($v[$entry])){
                        $newObj[$key][$entry]=$v[$entry];
                        $checkArray=false;
                    }
                }
            }
            if(is_array($v) && $checkArray)
                $newObj[$key]=self::checkCOstumList($v, $newObj[$key], $k);
        }
        return $newObj;
    }

    /*
    * @filterThemeInCustom permits to clean, update or add entry in paramsConfig of communecter thanks to costumParams 
    *	Receive Yii::app()->session["paramsConfig"] in order to modify it if Yii::app()->session["costum"] is existed
    *	
    */
    public static function filterThemeInCustom($params){
        //filter menu app custom 
        // Html construction of communecter
        if(isset(Yii::app()->session["costum"]["htmlConstruct"])){
            $constructParams=Yii::app()->session["costum"]["htmlConstruct"];
            //var_dump($params);exit;
            
            // Check about header construction
            // - Entry banner to adda tpl path to a custom banner, by default not isset
            // - Entry menuTop will config btn present in menu top (navLeft & navRight)
            if(isset($constructParams["header"])){
                if(isset($constructParams["header"]["banner"])) //URL
                    $params["header"]["banner"]=$constructParams["header"]["banner"];
                if(isset($constructParams["header"]["css"]))
                    $params["header"]["css"]=$constructParams["header"]["css"];
                if(isset($constructParams["header"]["menuTop"])){
                    if(isset($constructParams["header"]["menuTop"]["navLeft"]))
                        $params["header"]["menuTop"]["navLeft"]=self::checkCOstumList($constructParams["header"]["menuTop"]["navLeft"],$params["header"]["menuTop"]["navLeft"]);
                    if(isset($constructParams["header"]["menuTop"]["navRight"])){
                        $params["header"]["menuTop"]["navRight"]=self::checkCOstumList($constructParams["header"]["menuTop"]["navRight"],$params["header"]["menuTop"]["navRight"]);
                    }
                }
            }
            if(isset($constructParams["subMenu"])){
                if(empty($constructParams["subMenu"]))
                    $params["subMenu"]=false;
                else
                    $params["subMenu"]=self::checkCOstumList($constructParams["subMenu"],$params["subMenu"]);
            }
            if(isset($constructParams["footer"])){
                if(!empty($constructParams["footer"]))
                    $params["footer"]=self::checkCOstumList($constructParams["footer"],$params["footer"]);
                else
                    $params["footer"]=$constructParams["footer"];
            }
            
            // Check about admin Panel navigation (cosstumized space for costum administration expectations)
            if(isset($constructParams["adminPanel"]))
                $params["adminPanel"]=self::checkCOstumList($constructParams["adminPanel"],$params["adminPanel"]);
            // if(isset($constructParams["directory"]))
              //  $params["directory"]=self::checkCOstumList($constructParams["directory"],$params["directory"]);
                
            // Check about element page organization and content
            // - #initView is a param to give first view loaded when arrived in a page (ex : newstream, detail, agenda or another)
            // - #menuLeft of element => TODO PLUG ON checkCOstumList
            // - #menuTop of element => TODO finish customization & add it in params.json
            if(isset($constructParams["element"])){
                if(isset($constructParams["element"]["initView"]))
                    $params["element"]["initView"]=$constructParams["element"]["initView"];
                if(isset($constructParams["element"]["banner"])){
                    $params["element"]["banner"]=self::checkCOstumList($constructParams["element"]["banner"],$params["element"]["banner"]);
                }
                if(isset($constructParams["element"]["menuLeft"])){
                    $params["element"]["menuLeft"]=self::checkCOstumList($constructParams["element"]["menuLeft"],$params["element"]["menuLeft"]);
                }
               
            }
            // #appRendering permit to get horizontal and vertical organization of menu 
            //	 - horizontal = first version of co2 with menu of apps in horizontal 
            //	 - vertical = current version of co2 with menu of apps on the left 
            if(isset($constructParams["appRendering"]))
                $params["appRendering"]=$constructParams["appRendering"];
            if(isset($constructParams["loadingModal"]))
                $params["loadingModal"]=$constructParams["loadingModal"];
            
            if(isset($constructParams["directory"])){
                //var_dump($params["directory"]);
                $directoryP=$params["directory"];
                //var_dump($directoryP);
                $params["directory"]=$constructParams["directory"];
                //var_dump($params["directory"]);
                foreach($directoryP as $k => $v){
                    if(!isset($params["directory"][$k]))
                        $params["directory"][$k]=$v;
                }
            }

            // # redirect permits to application in case of undefined view or error or automatic redirection to fall on costum redirect
            // To be setting for logged user && unlogged user
            if(isset($constructParams["redirect"]))
                $params["pages"]["#app.index"]["redirect"]=$constructParams["redirect"];
            if(isset($constructParams["menuBottom"]))
                $params["menuBottom"]=self::checkCOstumList($constructParams["menuBottom"],$params["menuBottom"]);
        }
        // Check about app (#search, #live, #etc) if should costumized
        $menuApp = array("#annonces", "#search", "#agenda", "#live", "#dda", "#docs");

        if(isset(Yii::app()->session["costum"]["app"])){
            $params["numberOfApp"]=count(Yii::app()->session["costum"]["app"]);
            $menuPages=self::checkCOstumList(Yii::app()->session["costum"]["app"], $params["pages"]);
           //Rest::json($menuPages); exit ;
          //var_dump($menuPages); exit ;
            //Rest::json($menuPages); exit ;
            foreach($params["pages"] as $hash => $v){
                if(!in_array($hash,$menuApp))
                    $menuPages[$hash]=$v;
            }
            $params["pages"]=$menuPages;
            //Rest::json($menuPages); exit ;
        }
        if(isset(Yii::app()->session["costum"]["typeObj"]))
            $params["typeObj"]=Yii::app()->session["costum"]["typeObj"];
        return $params;
    }
    public static function getContextList($slug, $contextName){

        $layoutPath ="../../modules/costum/json/".$slug."/".$contextName.".json";

        $str = file_get_contents($layoutPath);
        $list = json_decode($str, true);
        return $list;
    }

     /*
    * @checkUserPreferences will check for a costum presence of user settings
    * Example : Geographical selction to oriented a user experience (cf : @meuseCampagnes)
    * String $userId to get preferences 
    * Array $costum to find specific user preferences
    * Return costum array with or without user settings about this costum 
    */
    public static function checkUserPreferences($costum, $userId){
        $userPref=Preference::getPreferencesByTypeId($userId, Person::COLLECTION);
        if(!empty($userPref) && isset($userPref["costum"]) && isset($userPref["costum"][$costum["slug"]])){
            $costum["userPreferences"]=$userPref["costum"][$costum["slug"]];
        }
        return $costum;
    }
    /*
    *   @initMetaData will initialize variable to get set metaDatas in mainSearch.php
    *   Meta is composed of title, description, keywords, image and author
    */
    public static function initMetaData($c){

        if(isset($c["metaDesc"]))
            $shortDesc=$c["metaDesc"];
        else{
            $shortDesc =  @$c["shortDescription"] ? $c["shortDescription"] : "";
                if($shortDesc=="")
                    $shortDesc = @$c["description"] ? $c["description"] : "";
        }
        $this->getController()->module->description = $shortDesc;
        $this->getController()->module->pageTitle = (isset($c["metaTitle"])) ? $c["metaTitle"] : @$c["title"];
        if(isset($c["metaKeywords"]))
            $this->getController()->module->keywords = $c["metaKeywords"];
        else
           $this->getController()->module->keywords = (@$c["tags"]) ? implode(",", @$c["tags"]) : "";
        if (isset($c["favicon"])) {
            $mod = $this->getController()->module->id;
            //ex images can be given 
            if( substr_count($c["favicon"], '#') ){
                $pieces = explode("#", $c["favicon"]);
                $mod = $pieces[0];
                $c["favicon"] = $pieces[1];
            }
            $this->getController()->module->favicon = Yii::app()->getModule( $mod )->getAssetsUrl().$c["favicon"];
        }
        if (@$c["metaImg"]) {
            $mod = $this->getController()->module->id;
            //ex images can be given 
            if( substr_count($c["metaImg"], '#') ){
                $pieces = explode("#", $c["metaImg"]);
                $mod = $pieces[0];
                $c["metaImg"] = $pieces[1];
            }
            $this->getController()->module->image = Yii::app()->getModule( $mod )->getAssetsUrl().$c["metaImg"];
        }
        else if(@$c["logo"]){
            $this->getController()->module->image = $c["logo"];
        }
        //if(@)

    }
    /*
    *   @initScopeSelector will init geographical object of selection and user choice in costum
    *   return costum with entry necessary :
    *   Scopeseclector with entiere specifity of scope
    *   UserPreferences refering to its costum geographical experience
    */
    public static function initScopeSelector($params){
        $params["scopeSelector"]=Zone::getScopeByIds($params["scopeSelector"])["scopes"];
        /*foreach(["cities", "cp", "zones"] as $impact){
            if(isset($params["userPreferences"]) && isset($params["userPreferences"][$impact])){
                foreach($params["userPreferences"][$impact] as $key){
                    $valueScope=$params["scopeSelector"][$key];
                    $idScope=$params["scopeSelector"][$key]["id"];
                }
                //$params["filters"]=(!isset($params["filters"])) ? [] : $params["filters"];
                //$params["filters"]["scopes"][$impact]=$idScope;         
            }
        }*/
        return $params;
    }
    public static function prepData($params){
        if(!empty(Yii::app()->session["costum"] ) ) {

            if( !empty($params["source"]) && !empty($params["source"]["toBeValidated"]) ) {
                $validated = true;
                if( !empty(Yii::app()->session["costum"]["typeObj"]) && 
                    ( (!empty(Yii::app()->session["costum"]["typeObj"][$params["collection"]]) &&
                        isset(Yii::app()->session["costum"]["typeObj"][$params["collection"]]["validatedParent"]) &&
                        Yii::app()->session["costum"]["typeObj"][$params["collection"]]["validatedParent"]) ||
                        (@$params["key"] && !empty(Yii::app()->session["costum"]["typeObj"][$params["key"]]) &&
                        isset(Yii::app()->session["costum"]["typeObj"][$params["key"]]["validatedParent"]) && Yii::app()->session["costum"]["typeObj"][$params["key"]]["validatedParent"]) ) ) {

                    if($params["collection"] == Event::COLLECTION && !empty($params["organizer"])) {
                        $parent = "organizer";
                    }else{
                        $parent = "parent";
                    }
                    $validated = false;
                    foreach ($params[$parent] as $key => $value) {
                        if(!empty($value["type"])){
                            $eltParent = PHDB::findOneById($value["type"],$key, array('name','source'));
                            // var_dump( $eltParent);
                            if( !empty($eltParent) && 
                                !empty($eltParent["source"]) && 
                                !empty($eltParent["source"]["toBeValidated"]) && 
                                !empty($eltParent["source"]["toBeValidated"][Yii::app()->session["costum"]["slug"]]) &&
                                $eltParent["source"]["toBeValidated"][Yii::app()->session["costum"]["slug"]] )
                                $validated = true;
                        }else
                            $validated = true;
                        
                    }
                }
                //var_dump( $validated);
                if($validated === true)
                    $params["source"]["toBeValidated"] = array(Yii::app()->session["costum"]["slug"] => true);
            }


        }


        if(!empty(Yii::app()->session["costum"]) && Yii::app()->session["costum"]["slug"] == "siteDuPactePourLaTransition"){


            if($params["collection"] == Action::COLLECTION){
            	if(!empty($params["measures"] )){
            		foreach ($params["measures"] as $key => $value) {
	                	$params["name"] = $value["name"];
	                }
            	}
                
            }
            
        }
        return $params;
    }

    public static function value($value){
        if( substr_count($value , 'this.') > 0 ){
            $field = explode( ".", $value );
            if( isset( Yii::app()->session["costum"][ $field[1] ] ) )
                return Yii::app()->session["costum"][ $field[1] ];
        }
        else 
            return Yii::app()->session["costum"][ $value ];
    }
}
?>